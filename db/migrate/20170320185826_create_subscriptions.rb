class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|

      t.integer :company_id, null: false, index: true
      t.integer :plan_id, null: false, index: true
      t.float :price
      t.datetime :expiry
      t.boolean :status, default: true
      t.timestamps null: false
    end
  end
end
