class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|

      t.integer :attachable_id
      t.string :attachable_type
      t.boolean :main_image
      t.timestamps null: false
    end
  end
end
