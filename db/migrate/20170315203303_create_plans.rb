class CreatePlans < ActiveRecord::Migration
  def change
    create_table :plans do |t|
      t.string :name
      t.text :description
      t.integer :plan_type, default: 0
      t.integer :max_adds
      t.float :price
      t.datetime :expiry
      t.boolean :status, default: true
      t.timestamps null: false
    end
  end
end
