class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|

      t.string :name
      t.text :description
      t.boolean :status, default: true
      t.integer :company_id, null: false, index: true
      t.timestamps null: false
    end
  end
end
