class AddCompanyIdToAdminUser < ActiveRecord::Migration
  def change
    add_column :admin_users, :company_id, :integer, index: true
  end
end
