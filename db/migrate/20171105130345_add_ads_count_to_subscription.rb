class AddAdsCountToSubscription < ActiveRecord::Migration
  def change
    add_column :subscriptions, :ads_count, :integer
  end
end
