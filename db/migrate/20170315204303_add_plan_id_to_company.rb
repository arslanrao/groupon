class AddPlanIdToCompany < ActiveRecord::Migration
  def change
    add_column :companies, :plan_id, :integer, index: true
  end
end
