class CreateAds < ActiveRecord::Migration
  def change
    create_table :ads do |t|

      t.string :name
      t.text :description
      t.float :price
      t.integer :total_views, default: 0
      t.datetime :expiry
      t.boolean :status, default: true
      t.integer :company_id, null: false, index: true
      t.timestamps null: false
    end
  end
end
