class CreateCompanies < ActiveRecord::Migration
  def change
    create_table :companies do |t|
      t.string :name
      t.text :description
      t.string :phone
      t.boolean :status, default: true
      t.timestamps null: false
    end
  end
end
