class Plan < ActiveRecord::Base

  include ExpiryDateValidator

  has_one :company
  has_many :subscriptions

  enum plan_type: [ :silver, :gold , :platinum]

  validates :name, :description, :plan_type, :max_adds, :price, :expiry, :status, presence: true
  validates_length_of :name, minimum: 1, maximum: 250
  validates :plan_type, inclusion: { in: plan_types }
  validates :price, numericality: { less_than_or_equal_to: 9999999999 }
  validates :max_adds, numericality: { less_than_or_equal_to: 9999999999, only_integer: true }

end
