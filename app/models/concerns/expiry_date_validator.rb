module ExpiryDateValidator
  extend ActiveSupport::Concern

  included do
    validate :past_expiry_time
  end

  private

  def past_expiry_time
    if expiry.present? && expiry < Time.now
      errors.add(:expiry, "can't be in the past")
    end
  end
end
