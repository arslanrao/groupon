class Subscription < ActiveRecord::Base

  include ExpiryDateValidator

  belongs_to :company
  belongs_to :plan

  validates :expiry, :status, presence: true

end
