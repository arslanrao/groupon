class AdminUser < ActiveRecord::Base

  belongs_to :company
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable

  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i }

  ROLES = { super_admin: 'super_admin',  company_admin: 'company_admin' }.freeze

  def role?(base_role)
    return false unless role
    role == ROLES[base_role.to_sym]
  end

  def company_list
    companies = role?('super_admin') ? Company.all : [company]
    companies.map { |company| [company.name, company.id] }
  end

end
