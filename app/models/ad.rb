class Ad < ActiveRecord::Base

  include ExpiryDateValidator

  paginates_per 25

  after_create :update_subscription

  belongs_to :company
  belongs_to :category
  has_many :photos, as: :attachable
  accepts_nested_attributes_for :photos, allow_destroy: true

  validates :name, :description, :price, :expiry, :status, presence: true
  validates_length_of :name, minimum: 1, maximum: 250
  validates :price, numericality: { less_than_or_equal_to: 9999999999 }
  validate :plan_limits

  private

  def plan_limits
    subscription = company.current_subscription
    self.errors.add(:limit, 'Ads limit reached for current subscription!') if subscription.ads_count <= 0
  end

  def update_subscription
    subscription = company.current_subscription
    subscription.decrement!(:ads_count)
  end

end
