class Category < ActiveRecord::Base

  has_many :ads

  validates :name, :description, presence: true
  validates_length_of :name, minimum: 1, maximum: 250

end
