class User < ActiveRecord::Base

  has_many :user_identities

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates :first_name, :last_name, :email, presence: true
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i}
  validates_length_of :first_name, :last_name, :email, minimum: 1, maximum: 250

  def generate_access_token(device_id)
    user_identity = user_identities.find_or_create_by(device_id: device_id)
    user_identity.access_token
  end

end
