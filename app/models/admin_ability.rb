# All back end users (i.e. Active Admin users) are authorized using this class
class AdminAbility
  include CanCan::Ability

  def initialize(user)
    if user && user.class == AdminUser
      can :manage, ActiveAdmin::Page

      if user.role?('super_admin')
        can :manage, :all
      elsif user.role?('company_admin')
        can :create, Ad
        can :manage, Ad, company_id: user.company.try(:id).to_i
        can :read, Company, id: user.company.id
        can :update, Company, id: user.company.id
      end
    end
  end
end
