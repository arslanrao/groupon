class Company < ActiveRecord::Base

  attr_accessor :email
  after_create :invite_admin
  after_create :create_subscription

  has_many :ads
  has_many :subscriptions
  has_one :admin_user
  belongs_to :plan

  validates :name, :description, :phone, :email, presence: true
  validates_length_of :name, :phone, minimum: 1, maximum: 250
  validates :phone, numericality: { only_integer: true }
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
  validate :verify_admin_user, on: :create

  def admin_name
    admin_user = self.admin_user
    admin_user.email if admin_user
  end

  def current_subscription
    subscriptions.where('expiry > ?', Time.now).first
  end

  private

  def invite_admin
    AdminUser.invite!(email: self.email, company_id: self.id, role: AdminUser::ROLES[:company_admin])
  end

  def verify_admin_user
    admin_user = AdminUser.find_by(email: email)
    self.errors.add(:email, 'Admin with this email is already present') if admin_user
  end

  def create_subscription
    self.subscriptions.create(plan_id: self.plan.id, expiry: (Time.now + 1.month), ads_count: self.plan.max_adds)
  end

end
