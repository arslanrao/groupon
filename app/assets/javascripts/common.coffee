$(document).on 'ready turbolinks:load', ->
  $('#date_time_picker input').appendDtpicker
    'minDate': new Date($.now())
    'closeOnSelected': true
    'calendarMouseScroll': false
