class Api::V1::AdsController < ApiController
  include Concerns::UserAuthentication

  before_action :set_ad, only: [:show, :update, :destroy]
  load_and_authorize_resource

  resource_description do
    short "An ad is anything that companies sell, could be product or a deal"
  end

  def_param_group :ad_params do
    param :ad, Hash, required: true do
      param :name, String, required: true
      param :description, String, required: true
      param :price, Float, required: true
      param :expiry, DateTime, required: true
      param :company_id, Integer, required: true
    end
  end

  api :GET, '/ads', "Get all ads"
  formats ['json']
  description 'Return all ads'
  def index
    @ads = Ad.all
    render json: @ads, root: 'ads', adapter: :json
  end

  api :GET, '/ads/:id', "Get specific ad"
  formats ['json']
  description 'Return specific ad'
  def show
    success(data: AdSerializer.new(@ad))
  end

  api :POST, '/ads', "Create new ad"
  param_group :ad_params
  formats ['json']
  description 'Create a new ad'
  def create
    @ad = Ad.new(ad_params)

    if @ad.save
      created(data: AdSerializer.new(@ad))
    else
      unprocessable_entity(@ad.errors)
    end
  end

  api :PATCH, '/ads/:id', "Update a specific ad"
  param_group :ad_params
  formats ['json']
  description 'Update a specific ad'
  def update
    if @ad.update(ad_params)
      success(data: AdSerializer.new(@ad))
    else
      unprocessable_entity(@ad.errors)
    end
  end

  api :DELETE, '/ads/:id', "Delete specific ad"
  formats ['json']
  description 'Delete a specific ad'
  def destroy
    if @ad.destroy
      success
    else
      internal_server_error
    end
  end

  private

  def ad_params
    params.require(:ad).permit(:name, :description, :price, :expiry, :company_id, photos_attributes: [:avatar, :id, :main_image, :_destroy])
  end

  def set_ad
    @ad = Ad.find_by(id: params[:id])
    not_found('Ad not found') if @ad.blank?
  end

end
