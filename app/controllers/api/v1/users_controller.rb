class Api::V1::UsersController < ApiController
  include Concerns::UserAuthentication

  skip_before_action :authenticate_user, only: [:reset_password]
  skip_before_action :set_user, only: [:reset_password]
  before_action :set_user_object, only: [:reset_password]
  load_and_authorize_resource

  resource_description do
    short "A user register itself, login and purchases ads"
  end

  def_param_group :reset_password_params do
    param :email, String, required: true
  end

  api :POST, '/users/reset_password', "Reset password of given user"
  param_group :reset_password_params
  formats ['json']
  description 'Reset password of given user'
  def reset_password
    if @user.send_reset_password_instructions
      head :ok
    else
      head :not_acceptable
    end
  end

  private

  def set_user_object
    @user = User.find_by(email: params[:email])
    not_found('User not found') if @user.blank?
  end

end
