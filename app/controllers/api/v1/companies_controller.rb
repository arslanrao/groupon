class Api::V1::CompaniesController < ApiController
  include Concerns::UserAuthentication

  before_action :set_company, only: [:show, :update, :destroy]
  load_and_authorize_resource

  resource_description do
    short "A company post ads that users buy, depending upon its plan"
  end

  def_param_group :company_params do
    param :company, Hash, required: true do
      param :name, String, required: true
      param :description, String, required: true
      param :phone, String, required: true
      param :email, String, required: true
    end
  end

  api :GET, '/companies', "Get all companies"
  formats ['json']
  description 'Return all companies'
  def index
    @companies = Company.all
    render json: @companies, root: 'companies', adapter: :json
  end

  api :GET, '/companies/:id', "Get specific company"
  formats ['json']
  description 'Return specific company'
  def show
    success(data: CompanySerializer.new(@company))
  end

  api :POST, '/companies', "Create new company"
  param_group :company_params
  formats ['json']
  description 'Create a new company'
  def create
    @company = Company.new(company_params)

    if @company.save
      created(data: CompanySerializer.new(@company))
    else
      unprocessable_entity(@company.errors)
    end
  end

  api :PATCH, '/companies/:id', "Update a specific company"
  param_group :company_params
  formats ['json']
  description 'Update a specific company'
  def update
    if @company.update(company_params)
      success(data: CompanySerializer.new(@company))
    else
      unprocessable_entity(@company.errors)
    end
  end

  api :DELETE, '/companies/:id', "Delete specific company"
  formats ['json']
  description 'Delete a specific company'
  def destroy
    if @company.destroy
      success
    else
      internal_server_error
    end
  end

  private

  def company_params
    params.require(:company).permit(:name, :description, :phone, :email)
  end

  def set_company
    @company = Company.find_by(id: params[:id])
    not_found('Company not found') if @company.blank?
  end

end
