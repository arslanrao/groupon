class Api::V1::CategoriesController < ApiController
  include Concerns::UserAuthentication

  before_action :set_category, only: [:show, :update, :destroy]
  before_action :set_category_by_category_id, only: [:ads]
  load_and_authorize_resource

  resource_description do
    short "Category is defined as same type of ads, An ad must belong to one category"
  end

  def_param_group :category_params do
    param :category, Hash, required: true do
      param :name, String, required: true
      param :description, String, required: true
    end
  end

  api :GET, '/categories', "Get all categories"
  formats ['json']
  description 'Return all categories'
  def index
    @categories = Category.all
    render json: @categories, root: 'categories', adapter: :json
  end

  api :GET, '/categories/:id', "Get specific category"
  formats ['json']
  description 'Return specific category'
  def show
    success(data: CategorySerializer.new(@category))
  end

  api :POST, '/categories', "Create new category"
  param_group :category_params
  formats ['json']
  description 'Create a new category'
  def create
    @category = Category.new(category_params)

    if @category.save
      created(data: CategorySerializer.new(@category))
    else
      unprocessable_entity(@category.errors)
    end
  end

  api :PATCH, '/categories/:id', "Update a specific category"
  param_group :category_params
  formats ['json']
  description 'Update a specific category'
  def update
    if @category.update(category_params)
      success(data: CategorySerializer.new(@category))
    else
      unprocessable_entity(@category.errors)
    end
  end

  api :DELETE, '/categories/:id', "Delete specific category"
  formats ['json']
  description 'Delete a specific category'
  def destroy
    if @category.destroy
      success
    else
      internal_server_error
    end
  end

  api :GET, '/categories/:id/ads', "Get ads of a specific category"
  formats ['json']
  description 'Get ads of a specific category, send page number as param, number of max records per page is 25'
  def ads
    render json: @category.ads.page(params[:page]), adapter: :json
  end

  private

  def category_params
    params.require(:category).permit(:name, :description)
  end

  def set_category
    @category = Category.find_by(id: params[:id])
    not_found('Category not found') if @category.blank?
  end

  def set_category_by_category_id
    @category = Category.find_by(id: params[:category_id])
    not_found('Category not found') if @category.blank?
  end

end
