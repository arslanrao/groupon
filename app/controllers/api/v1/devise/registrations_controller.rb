class Api::V1::Devise::RegistrationsController < Devise::RegistrationsController
  include Concerns::UserAuthentication

  skip_before_action :authenticate_user, only: [:create]
  skip_before_action :set_user, only: [:create]
  before_action :validate_signup_params, only: [:create]

  resource_description do
    short "Handle user's registrations process"
  end

  def_param_group :sign_up_params_group do
    param :user, Hash do
      param :email, String
      param :password, String
      param :device_id, String
      param :first_name, String
      param :last_name, String
    end
  end

  api :POST, '/users', "User Sign-Up"
  param_group :sign_up_params_group
  formats ['json']
  description 'User can sign up'
  def create
    resource = User.new sign_up_params
    if resource.save
      access_token = resource.generate_access_token(params[:user][:device_id])
      if access_token.present?
        sign_in(:user, resource)
        return render json: {success: true, access_token: access_token}, status: :created
      else
        render json: {success: false}, status: :internal_server_error
      end
    else
      warden.custom_failure!
      render json: {success: false, errors: resource.errors}, status: :unauthorized
    end
  end

  private

  def sign_up_params
    signup_params = devise_parameter_sanitizer.sanitize(:sign_up)
    signup_params = {} if signup_params.blank?

    signup_params[:email] = params[:user][:email]
    signup_params[:first_name] = params[:user][:first_name]
    signup_params[:last_name] = params[:user][:last_name]
    signup_params[:password] = params[:user][:password]
    signup_params
  end

  def invalid_signup_attempt error=nil
    error = 'Invalid signup attempt' if error.blank?
    render json: {success: false, error: error}, status: :unauthorized
  end

  def validate_signup_params
    begin
      invalid_signup_attempt if sign_up_params.blank? || params[:user][:device_id].blank?
    rescue ActionController::ParameterMissing
      invalid_signup_attempt
    end
  end

end
