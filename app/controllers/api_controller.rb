class ApiController < ActionController::Base
  include Api::ApiResponseHelper
  before_action :authenticate_request
  before_action :can_can

  rescue_from CanCan::AccessDenied do
    unauthorized
  end

  rescue_from ActionController::ParameterMissing do |exception|
    unprocessable_entity('Required params missing')
  end

  def can_can
    resource = controller_name.singularize.to_sym
    method = "#{resource}_params"
    params[resource] &&= send(method) if respond_to?(method, true)
  end

  private

  def authenticate_request
    token = "G8pfydyTmE4b3FmLEITjJxE86v2EuWAEIYogFjwTeD2YuE4HCZjF4oXoV3gfmL1YPTt3hT2jyqIR2RbF1PhDtjUXgOTRHL2nR3Cl50y3m6IYd3EbUu8F8S4sZrEYE1"
    return unauthorized("Invalid Authorization-Token") unless token == request.headers['HTTP_AUTHORIZATION']
  end

end
