ActiveAdmin.register Subscription do
  permit_params :price, :expiry, :status, :company_id, :plan_id

  index do
    selectable_column
    id_column
    column 'Company' do |a|
      link_to a.company.name, admin_company_path(a.company_id) if a.company
    end
    column 'Plan' do |a|
      link_to a.plan.name, admin_plan_path(a.plan_id) if a.plan_id
    end
    column :price
    column :expiry
    column :status
    column :created_at
    actions
  end

  filter :plan_id, as: :select, collection: Plan.all.map {|plan| [plan.name, plan.id]}, label: "Plan"
  filter :company_id, as: :select, collection: Company.all.map {|company| [company.name, company.id]}, label: "Company"
  filter :price
  filter :expiry
  filter :status
  filter :created_at

  form do |f|
    f.inputs "Subscription Details" do
      f.input :company_id, label: 'Company', as: :select, collection: Company.all.map {|company| [company.name, company.id]}, include_blank: false
      f.input :plan_id, label: 'Plan', as: :select, collection: Plan.all.map {|plan| [plan.name, plan.id]}, include_blank: false
      f.input :price
      f.input :expiry, as: :string, wrapper_html: { id: 'date_time_picker' }
      f.input :status
    end
    f.actions
  end
end
