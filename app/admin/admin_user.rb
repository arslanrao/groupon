ActiveAdmin.register AdminUser do
  permit_params :email, :password, :password_confirmation, :company_id

  index do
    selectable_column
    id_column
    column :email
    column 'Company' do |a|
      link_to a.company.name, admin_company_path(a.company_id) if a.company
    end
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :company_id, as: :select, collection: Company.all.map {|company| [company.name, company.id]}, label: "Company"
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  show do
    attributes_table do
      row :email
      row :company_id do |ad|
        link_to ad.company.name, admin_company_path(ad.company_id) if ad.company_id
      end
      row :created_at
      row :updated_at
    end
  end

  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :company_id, label: 'Company', as: :select, collection: Company.all.map {|company| [company.name, company.id]}, include_blank: false
      f.input :password
      f.input :password_confirmation
    end
    f.actions
  end

end
