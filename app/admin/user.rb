ActiveAdmin.register User do
  permit_params :first_name, :last_name, :email, :password, :status

  index do
    selectable_column
    id_column
    column :first_name
    column :last_name
    column :email
    column :sign_in_count
    column :last_sign_in_ip
    column :status
    column :last_sign_in_at
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :first_name
      row :last_name
      row :email
      row :status
      row :created_at
      row :updated_at
    end
  end

  filter :first_name
  filter :last_name
  filter :email
  filter :status
  filter :created_at

  form do |f|
    f.inputs "User Details" do
      f.input :first_name
      f.input :last_name
      f.input :email
      f.input :password
      f.input :status
    end
    f.actions
  end
end
