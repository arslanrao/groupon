ActiveAdmin.register Plan do
  permit_params :name, :description, :plan_type, :max_adds, :price, :expiry, :status

  index do
    selectable_column
    id_column
    column :name
    column :description
    column :plan_type
    column :max_adds
    column :price
    column :expiry
    column :status
    column :created_at
    actions
  end

  filter :name
  filter :description
  filter :plan_type
  filter :max_adds
  filter :price
  filter :expiry
  filter :status
  filter :created_at

  form do |f|
    f.inputs "Plan Details" do
      f.input :name
      f.input :description
      f.input :plan_type, include_blank: false
      f.input :max_adds
      f.input :price
      f.input :expiry, as: :string, wrapper_html: { id: 'date_time_picker' }
      f.input :status
    end
    f.actions
  end
end
