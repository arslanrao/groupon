ActiveAdmin.register Company do
  permit_params :name, :description, :phone, :status, :plan_id, :email

  filter :name
  filter :description
  filter :phone
  filter :plan_id, as: :select, collection: Plan.all.map {|plan| [plan.name, plan.id]}, label: "Plan"
  filter :status
  filter :created_at

  index do
    selectable_column
    id_column
    column :name
    column :description
    column :phone
    column 'Plan' do |a|
      link_to a.plan.name, admin_plan_path(a.plan_id) if a.plan
    end
    column :status
    column 'Admin' do |company|
      company.admin_name
    end
    column :created_at
    actions
  end

  show do
    panel 'Company Details' do
      attributes_table_for resource do
        row :name
        row :description
        row :phone
        row :status
        row 'Plan' do |company|
          link_to company.plan.name, admin_plan_path(company.plan_id) if company.plan
        end
        row 'Admin' do |company|
          company.admin_name
        end
        row :created_at
        row :updated_at
      end
    end
  end

  form do |f|
    f.inputs "Company Details" do
      f.input :name
      f.input :description
      f.input :phone
      f.input :plan_id, label: 'Plan', as: :select, collection: Plan.all.map {|plan| [plan.name, plan.id]}, include_blank: false
      if f.object.new_record?
        f.input :email, label: 'Admin Email'
      else
        f.li "<label>Admin Email</label><span class='field-value'>#{f.object.admin_name || 'N/A'}</span>".html_safe
      end
      f.input :status
    end
    f.actions
  end

end
