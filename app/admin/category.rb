ActiveAdmin.register Category do
  permit_params :name, :description, :status

  index do
    selectable_column
    id_column
    column :name
    column :description
    column :status
    column :created_at
    actions
  end

  filter :name
  filter :description
  filter :status
  filter :created_at

  form do |f|
    f.inputs "Category Details" do
      f.input :name
      f.input :description
      f.input :status
    end
    f.actions
  end
end
