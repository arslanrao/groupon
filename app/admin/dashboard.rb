ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    panel "Welcome to Addpedia" do
      columns do
        column do
          panel "Recent Ads" do
            ul do
              Ad.last(5).map do |ad|
                li link_to(ad.name, admin_ad_path(ad))
              end
            end
          end
        end

        column do
          panel "Recent Users" do
            ul do
              User.last(5).map do |user|
                li link_to(user.email, admin_user_path(user))
              end
            end
          end
        end
      end
    end
  end
end
