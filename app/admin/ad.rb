ActiveAdmin.register Ad do
  permit_params :name, :description, :price, :total_views, :expiry, :status, :company_id, :category_id, photos_attributes: [:avatar, :id, :main_image, :_destroy]

  after_create do |ad|
    flash.now[:alert] = ad.errors[:limit] if ad.errors[:limit].present?
  end

  index do
    selectable_column
    id_column
    column :name
    column :description
    column :price
    column :total_views
    column 'Company' do |a|
      link_to a.company.name, admin_company_path(a.company_id) if a.company
    end
    column 'Category' do |a|
      link_to a.category.name, admin_category_path(a.category_id) if a.category_id
    end
    column :expiry
    column :status
    column :created_at
    actions
  end

  show do |ad|
    attributes_table do
      row :name
      row :description
      row :price
      row "Company" do
        link_to(ad.company.name, admin_company_path(ad.company))
      end
      row "Category" do
        link_to(ad.category.name, admin_category_path(ad.category))
      end
      row :expiry
      row :status
      row "Photos" do
         ul do
          ad.photos.each do |photo|
            li do
              image_tag(photo.avatar.url(:thumb))
            end
            para "Main Image: #{photo.main_image}"
          end
         end
      end
    end
  end

  filter :name
  filter :description
  filter :price
  filter :total_views
  filter :company_id, as: :select, collection: Company.all.map {|company| [company.name, company.id]}, label: "Company"
  filter :category_id, as: :select, collection: Category.all.map {|category| [category.name, category.id]}, label: "Category"
  filter :expiry
  filter :status
  filter :created_at

  form do |f|
    f.inputs "Ad Details" do
      f.input :name
      f.input :description
      f.input :price
      f.input :company_id, label: 'Company', as: :select, collection: current_admin_user.company_list, include_blank: false
      f.input :category_id, label: 'Category', as: :select, collection: Category.all.map {|category| [category.name, category.id]}, include_blank: false
      f.input :expiry, as: :string, wrapper_html: { id: 'date_time_picker' }
      f.input :status
      f.has_many :photos, heading: 'Photos' do |p|
        hint = (p.object.avatar.nil? || p.object.new_record?) ? p.template.content_tag(:span, 'No photo yet') : p.template.image_tag(p.object.avatar.url(:thumb))

        p.input :avatar, label: 'Photo', hint: hint
        p.input :main_image, as: :boolean, required: false, label: 'Main image'
        p.input :_destroy, as: :boolean, required: false, label: 'Remove image'
      end
    end
    f.actions
  end
end
