class AdSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :price, :total_views, :expiry, :status, :photos


  def photos
    result = []

    if object.photos.present?
      object.photos.each do |photo|
        photo_obj = {}
        photo_obj[:main_image] = photo.main_image

        Photo::STYLES.each do |size|
          photo_obj[size] = [ActionController::Base.asset_host, photo.avatar.url(size)].join()
        end

        result << photo_obj
      end
    else
      photo_obj = {}
      photo_obj[:main_image] = true

      Photo::STYLES.each do |size|
        photo_obj[size] = [ActionController::Base.asset_host, Photo.new.avatar.url(size)].join()
      end

      result << photo_obj
    end

    result
  end
end
