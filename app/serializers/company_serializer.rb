class CompanySerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :phone, :status
end
